<?php

namespace Oreo;

use Oreo\Collections\Grupos;
use Oreo\Collections\Marcas;
use Oreo\Collections\Modelos;
use Oreo\Collections\Produtos;

class Parser
{
    private $stock_iterator;
    private $stock_grid_iterator;
    private $produtos;
    private $categorias;
    private $aplicacoes;

    public function __construct($path)
    {
        $this->stock_iterator = new CsvFileIterator($path . '/Estoque.txt');
        $this->stock_grid_iterator = new CsvFileIterator($path . '/Estoque_Grade.txt');
        $this->produtos = new Produtos();
        $this->grupos = new Grupos();
        $this->marcas = new Marcas();
        $this->modelos = new Modelos();
        $this->categorias = new Collection();
        $this->aplicacoes = new Collection();
        $this->cores = new Collection();
        $this->tamanhos = new Collection();


        $this->parseStock();
    }

    public function parseStock()
    {
        foreach ($this->stock_iterator as $row) {
            $this->parseStockRow($row);
        }
        foreach ($this->stock_grid_iterator as $row) {
            $this->parseStockGridRow($row);
        }
    }

    public function parseStockRow($row)
    {
        $row = array_map('utf8_encode', $row);
        $row = array_map('trim', $row);
        list(
            $codigo,
            $titulo,
            $preco,
            $unidade,
            $grupo_titulo,
            $subgrupo_titulo,
            $categoria_titulo,
            $aplicacao_titulo,
            $peso,
            $altura,
            $largura,
            $comprimento,
            $observacoes,
            $foto,
            $categoria_codigo,
            $familia,
            $estoque
        ) = $row;

        $produto = $this->produtos->addItem($codigo);
        $produto->titulo = $titulo;
        $produto->preco = (float) str_replace(',', '.', str_replace('.', '', $preco));
        $produto->unidade = $unidade;

        if ('' != $grupo_titulo) {
            $grupo = $this->grupos->addItem($grupo_titulo);
            $produto->grupo_slug = $grupo->slug;
            if ('' != $subgrupo_titulo) {
                $subgrupo = $grupo->subgrupos->addItem($subgrupo_titulo);
                $produto->subgrupo_slug = $subgrupo->slug;
            }
        }

        $categoria_titulo = preg_replace('#^\d+\s+#', '', $categoria_titulo);

        if ('' != $categoria_titulo) {
            $categoria = $this->categorias->addItem($categoria_titulo . ' ' . $categoria_codigo);
            $categoria->titulo = $categoria_titulo;
            $categoria->codigo = $categoria_codigo;
            if ('' != $grupo_titulo) {
                $categoria->grupo_slug = $grupo->slug;
                if ('' != $subgrupo_titulo) {
                    $categoria->subgrupo_slug = $subgrupo->slug;
                }
            }
            $produto->categoria_slug = $categoria->slug;
        }

        if ('' != $aplicacao_titulo) {
            $aplicacao = $this->aplicacoes->addItem($aplicacao_titulo);
            $produto->aplicacao_slug = $aplicacao->slug;
        }

        //trigger_error(print_r($familia, 1));

        $marca_modelo = explode("|", $familia);
        //trigger_error(print_r($marca_modelo, 1));
        $destaque = '';
        if (!empty($marca_modelo)) {

            if (isset($marca_modelo[0])) {
                $marca_titulo = trim($marca_modelo[0]);
            }
            if (isset($marca_modelo[1])) {
                $modelo_titulo = trim($marca_modelo[1]);
            }
            if (isset($marca_modelo[2])) {
                $destaque = trim($marca_modelo[2]);
            }
        }

        //trigger_error(print_r($marca_titulo, 1));

        if (isset($marca_titulo) && "" != $marca_titulo) {
            $marca = $this->marcas->addItem($marca_titulo);
            //echo $marca;

            $produto->marca_slug = $marca->slug;
        }

        if (isset($modelo_titulo) && "" != $modelo_titulo) {
            $modelo = $this->modelos->addItem($modelo_titulo);
            $produto->modelo_slug = $modelo->slug;
        }


        $produto->peso = (float) str_replace(',', '.', str_replace('.', '', $peso));
        $produto->altura = (float) str_replace(',', '.', str_replace('.', '', $altura));
        $produto->largura = (float) str_replace(',', '.', str_replace('.', '', $largura));
        $produto->comprimento = (float) str_replace(',', '.', str_replace('.', '', $comprimento));
        $produto->destaque=0;

        if ($destaque=="@") {
            $produto->destaque=1;
        }
        $produto->observacoes = $observacoes;
        $produto->foto = $foto;
        $produto->familia = $familia;
        $produto->estoque = (int) $estoque;

        return $produto;
    }

    public function parseStockGridRow($row)
    {
        $row = array_map('utf8_encode', $row);
        $row = array_map('trim', $row);
        list(
            $codigo,
            $cor_codigo,
            $cor_titulo,
            $tamanho_titulo,
            $titulo,
            $preco,
            $unidade,
            $grupo_titulo,
            $subgrupo_titulo,
            $categoria_titulo,
            $aplicacao_titulo,
            $peso,
            $altura,
            $largura,
            $comprimento,
            $observacoes,
            $foto,
            $categoria_codigo,
            $familia,
            $estoque
        ) = $row;

        $categoria_titulo = preg_replace('#^\d+\s+#', '', $categoria_titulo);

        if ('' == $cor_titulo && '' == $tamanho_titulo) {
            return null;
        }

        $produto = $this->produtos->getItem($codigo);
        if (null === $produto) {
            return null;
        }

        $titulo = array();
        if ('' != $cor_titulo) {
            $cor = $this->cores->addItem($cor_titulo);
            $cor->codigo = $cor_codigo;
            $titulo[] = $cor_titulo;
        }
        if ('' != $tamanho_titulo) {
            $tamanho = $this->tamanhos->addItem($tamanho_titulo);
            $titulo[] = $tamanho_titulo;
        }

        $titulo = join(' - ', $titulo);

        $opcao = $produto->opcoes->addItem($titulo);
        if ('' != $cor_titulo) {
            $opcao->cor_slug = $cor->slug;
        }
        if ('' != $tamanho_titulo) {
            $opcao->tamanho_slug = $tamanho->slug;
        }
        $opcao->preco = $preco;
        $opcao->unidade = $unidade;
        //$opcao->peso = $peso;
        $opcao->peso = (float) str_replace(',', '.', str_replace('.', '', $peso));
        $opcao->altura = (float) str_replace(',', '.', str_replace('.', '', $altura));
        $opcao->largura = (float) str_replace(',', '.', str_replace('.', '', $largura));
        $opcao->comprimento = (float) str_replace(',', '.', str_replace('.', '', $comprimento));
        $opcao->estoque = (int) $estoque;
        return $opcao;
    }

    public function getProdutos()
    {
        return $this->produtos;
    }

    public function getAplicacoes()
    {
        return $this->aplicacoes;
    }

    public function getCategorias()
    {
        return $this->categorias;
    }

    public function getCores()
    {
        return $this->cores;
    }

    public function getGrupos()
    {
        return $this->grupos;
    }

    public function getMarcas()
    {
        return $this->marcas;
    }

    public function getModelos()
    {
        return $this->modelos;
    }

    public function getTamanhos()
    {
        return $this->tamanhos;
    }
}
