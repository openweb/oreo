<?php

namespace Oreo\Models;

use Oreo\Collection;

class Grupo {
    public $subgrupos;
    public $titulo;
    public $slug;

    public function __construct() {
        $this->subgrupos = new Collection();
    }
}