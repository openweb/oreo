<?php

namespace Oreo\Models;

use Oreo\Collection;

class Produto
{
    public $codigo;
    public $titulo;
    public $preco;
    public $unidade;
    public $grupo_slug;
    public $marca_slug;
    public $subgrupo_slug;
    public $categoria_slug;
    public $aplicacao_slug;
    public $valor_antigo;
    public $peso;
    public $altura;
    public $largura;
    public $comprimento;
    public $observacoes;
    public $foto;
    public $estoque;
    public $opcoes;

    public function __construct()
    {
        $this->opcoes = new Collection();
    }
}
