<?php

namespace Oreo;

use Oreo\Collections\Grupos;
use Oreo\Collections\Produtos;

class Parser
{
    private $stock_iterator;
    private $stock_grid_iterator;
    private $produtos;
    private $categorias;
    private $aplicacoes;

    public function __construct($path)
    {
        $this->stock_iterator = new CsvFileIterator($path . '/Estoque.txt');
        $this->stock_grid_iterator = new CsvFileIterator($path . '/Estoque_Grade.txt');
        $this->produtos = new Produtos();
        $this->grupos = new Grupos();
        $this->categorias = new Collection();
        $this->aplicacoes = new Collection();
        $this->cores = new Collection();
        $this->tamanhos = new Collection();


        $this->parseStock();
    }

    public function parseStock()
    {
        foreach ($this->stock_iterator as $row) {
            $this->parseStockRow($row);
        }
        foreach ($this->stock_grid_iterator as $row) {
            $this->parseStockGridRow($row);
        }
    }

    public function parseStockRow($row)
    {
        $row = array_map('utf8_encode', $row);
        $row = array_map('trim', $row);
        list(
            $codigo,
            $titulo,
            $preco,
            $unidade,
            $grupo_titulo,
            $subgrupo_titulo,
            $categoria_titulo,
            $aplicacao_titulo,
            $peso,
            $altura,
            $largura,
            $comprimento,
            $observacoes,
            $foto,
            $categoria_codigo,
            $familia,
            $estoque
        ) = $row;

        $produto = $this->produtos->addItem($codigo);
        $produto->titulo = $titulo;
        $produto->preco = (float) str_replace(',', '.', str_replace('.', '', $preco));
        $produto->unidade = $unidade;

        if ('' != $grupo_titulo) {
            $grupo = $this->grupos->addItem($grupo_titulo);
            $produto->grupo_slug = $grupo->slug;
            if ('' != $subgrupo_titulo) {
                $subgrupo = $grupo->subgrupos->addItem($subgrupo_titulo);
                $produto->subgrupo_slug = $subgrupo->slug;
            }
        }

        $categoria_titulo = preg_replace('#^\d+\s+#', '', $categoria_titulo);

        if ('' != $categoria_titulo) {
            $categoria = $this->categorias->addItem($categoria_titulo . ' ' . $categoria_codigo);
            $categoria->titulo = $categoria_titulo;
            $categoria->codigo = $categoria_codigo;
            if ('' != $grupo_titulo) {
                $categoria->grupo_slug = $grupo->slug;
                if ('' != $subgrupo_titulo) {
                    $categoria->subgrupo_slug = $subgrupo->slug;
                }
            }
            $produto->categoria_slug = $categoria->slug;
        }

        if ('' != $aplicacao_titulo) {
            $aplicacao = $this->aplicacoes->addItem($aplicacao_titulo);
            $produto->aplicacao_slug = $aplicacao->slug;
        }



        $produto->peso = (float) str_replace(',', '.', str_replace('.', '', $peso));
        $produto->altura = (int) $altura;
        $produto->largura = (int) $largura;
        $produto->comprimento = (int) $comprimento;
        $produto->observacoes = $observacoes;
        $produto->foto = $foto;
        $produto->estoque = (int) $estoque;

        return $produto;
    }

    public function parseStockGridRow($row)
    {
        $row = array_map('utf8_encode', $row);
        $row = array_map('trim', $row);
        list(
            $codigo,
            $cor_codigo,
            $cor_titulo,
            $tamanho_titulo,
            $titulo,
            $preco,
            $unidade,
            $grupo_titulo,
            $subgrupo_titulo,
            $categoria_titulo,
            $aplicacao_titulo,
            $peso,
            $altura,
            $largura,
            $comprimento,
            $observacoes,
            $foto,
            $categoria_codigo,
            $familia,
            $estoque
        ) = $row;

        $categoria_titulo = preg_replace('#^\d+\s+#', '', $categoria_titulo);

        if ('' == $cor_titulo && '' == $tamanho_titulo) {
            return null;
        }

        $produto = $this->produtos->getItem($codigo);
        if (null === $produto) {
            return null;
        }

        $titulo = array();
        if ('' != $cor_titulo) {
            $cor = $this->cores->addItem($cor_titulo);
            $cor->codigo = $cor_codigo;
            $titulo[] = $cor_titulo;
        }
        if ('' != $tamanho_titulo) {
            $tamanho = $this->tamanhos->addItem($tamanho_titulo);
            $titulo[] = $tamanho_titulo;
        }

        $titulo = join(' - ', $titulo);

        $opcao = $produto->opcoes->addItem($titulo);
        if ('' != $cor_titulo) {
            $opcao->cor_slug = $cor->slug;
        }
        if ('' != $tamanho_titulo) {
            $opcao->tamanho_slug = $tamanho->slug;
        }
        $opcao->preco = $preco;
        $opcao->unidade = $unidade;
        $opcao->peso = $peso;
        $opcao->altura = $altura;
        $opcao->largura = $largura;
        $opcao->comprimento = $comprimento;
        $opcao->estoque = (int) $estoque;
        return $opcao;
    }

    public function getProdutos()
    {
        return $this->produtos;
    }

    public function getAplicacoes()
    {
        return $this->aplicacoes;
    }

    public function getCategorias()
    {
        return $this->categorias;
    }

    public function getCores()
    {
        return $this->cores;
    }

    public function getGrupos()
    {
        return $this->grupos;
    }

    public function getTamanhos()
    {
        return $this->tamanhos;
    }
}
