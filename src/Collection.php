<?php

namespace Oreo;

use Cocur\Slugify\Slugify;
use Countable;
use Iterator;
use JsonSerializable;
use Serializable;

class Collection implements Iterator, Countable, Serializable, JsonSerializable
{
    public $items = array();
    private $position = 0;

    public function serialize()
    {
        return serialize($this->items);
    }
    public function unserialize($serialized)
    {
        $this->items = unserialize($serialized);
    }

    public function jsonSerialize()
    {
        return $this->items;
    }

    public function getItems() {
        return $this->items;
    }

    public function addItem($titulo)
    {
        $item = $this->getItem($titulo);
        if (!$item) {
            $slugify = new Slugify();
            $item = new \stdClass();
            $item->titulo = $titulo;
            $item->slug = $slugify->slugify($titulo);
            $this->items[$item->slug] = $item;
        }
        return $item;
    }

    public function getItem($titulo)
    {
        $slugify = new Slugify();
        $slug = $slugify->slugify($titulo);
        return isset($this->items[$slug]) ? $this->items[$slug] : null;
    }

    public function current()
    { 
        $keys = array_keys($this->items);
        $key = $keys[$this->position];
        return $this->items[$key];
    }
    public function key()
    {
        return $this->position;
    }
    public function next()
    { 
        ++$this->position;
    }
    public function rewind()
    {
        $this->position = 0;
    }
    public function valid()
    { 
        $keys = array_keys($this->items);
        return isset($keys[$this->position]);
    }
    public function count() {
        return count($this->items);
    }
}
