<?php

namespace Oreo\Collections;

use Cocur\Slugify\Slugify;
use Oreo\Collection;


class Marcas extends Collection
{


    public function addItem($titulo)
    {
        $item = $this->getItem($titulo);
        if (!$item) {
            $slugify = new Slugify();
            $item = new Marcas();
            $item->titulo = $titulo;
            $item->slug = $slugify->slugify($titulo);
            $this->items[$item->slug] = $item;
        }
        return $item;
    }
}
