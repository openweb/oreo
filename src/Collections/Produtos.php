<?php

namespace Oreo\Collections;

use Oreo\Collection;
use Oreo\Models\Produto;

class Produtos extends Collection {
    

    public function addItem($codigo) {
        $item = $this->getItem($codigo);
        if (!$item) {
            $item = new Produto();
            $item->codigo = $codigo;
            $this->items[$item->codigo] = $item;
        }
        return $item;
    }

    public function getItem($codigo) {
        return isset($this->items[$codigo]) ? $this->items[$codigo] : null;
    }
}