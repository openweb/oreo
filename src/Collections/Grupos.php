<?php

namespace Oreo\Collections;

use Cocur\Slugify\Slugify;
use Oreo\Collection;
use Oreo\Models\Grupo;

class Grupos extends Collection {
    

    public function addItem($titulo) {
        $item = $this->getItem($titulo);
        if (!$item) {
            $slugify = new Slugify();
            $item = new Grupo();
            $item->titulo = $titulo;
            $item->slug = $slugify->slugify($titulo);
            $this->items[$item->slug] = $item;
        }
        return $item;
    }
}