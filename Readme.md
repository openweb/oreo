# Oreo

## Instalation
```
    composer config repositories.oreo vcs https://bitbucket.org/openweb/oreo
    composer require openweb/oreo
```

## Quickstart

### Usage
```php
<?php
    use Oreo\Parser as OreoParser;

    // path to the folder containing Estoque.txt and Estoque_Grade.txt
    $parser = new OreoParser("path_to_oreo_txt_files"); 

    $aplicacoes = $parser->getAplicacoes();
    print_r($aplicacoes);

    $categorias = $parser->getCategorias();
    print_r($categorias);

    $cores = $parser->getCores();
    print_r($cores);

    $grupos = $parser->getGrupos();
    foreach($grupos as $grupo) {
        print_r($grupo);
        $subgrupos = $grupo->subgrupos;
        print_r($subgrupos);
    }

    $tamanhos = $parser->getTamanhos();
    print_r($tamanhos);

    $produtos = $parser->getProdutos();
    foreach($produtos as $produto) {
        print_r($produto);
        $opcoes = $produto->opcoes;
        print_r($opcoes);
    }

```